#include <stddef.h>
#include <stdint.h>

void shake128_saber(uint8_t *output, size_t outlen, const uint8_t *input, size_t inlen);
