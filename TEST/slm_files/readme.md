# Test Stimuli
This folder contains all stimuli files that were used for testing.
Each Folder includes a spi_stim.txt file. This file has to be copied to TEST/slm_files and will be automatically recognized by the tools for simulation.
